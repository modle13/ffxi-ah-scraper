# download mozilla geckodriver

find target geckodriver at https://github.com/mozilla/geckodriver/releases

```
wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
tar -xvzf geckodriver-*
chmod +x geckodriver
sudo mv geckodriver /usr/local/bin/
rm geckodriver-*
```

# configure project

1. create a `users.txt` at the root of the project
2. each character you wish to query should be defined one per line in `users.txt`
3. update `run_once.sh` and `run.sh` with the correct path


`users.txt` example:
```
Characterone
Charactertwo
```

# install

```
python3 -m venv venv
. venv/bin/activate
pip install -r requirements/requirements.txt
```

# run

```
# to run on a 5 minute timer
./run.sh

# to run once
./run_once.sh
```
